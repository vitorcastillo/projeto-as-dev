import time
from datetime import datetime
import pyrebase
import os
import stat
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import random

firebaseConfig = {
    "apiKey": "AIzaSyBlUlBE_Nm95GMRSQfQdhpRlu8_1Xroj80",
    "authDomain": "as-grupo-sv.firebaseapp.com",
    "projectId": "as-grupo-sv",
    "databaseURL": "https://" + "as-grupo-sv" + ".firebaseio.com",
    "storageBucket": "as-grupo-sv.appspot.com",
    "messagingSenderId": "248857074739",
    "appId": "1:248857074739:web:10186e6202bb2b4ae01746",
    "measurementId": "G-D0LY1JRBJK"
}

firebase = pyrebase.initialize_app(firebaseConfig)
auth = firebase.auth()
server = "smtp.gmail.com"
port = 587
username = "vpedroso.vp@gmail.com"
passwordSMTP = "zablqualxseiiitn"

# (a) Solicitar que o usuário digite suas credencias (email e senha);
ok = True
while ok:
    print("1 - Cadastrar Usuário")
    print("2 - Verificar Email")
    print("3 - Autenticar Usuário")

    opcao = input("Selecione uma das opções:")

    if opcao == "1":
        user = input("Digite seu e-mail: ")
        password = input("Digite sua senha, com pelo menos 6 caracteres: ")
        status = auth.create_user_with_email_and_password(user, password)
        print("Email cadastrado: ",user)

    if opcao == "2":
        user = input("Digite seu e-mail: ")
        password = input("Digite sua senha, com pelo menos 6 caracteres: ")
        status = auth.sign_in_with_email_and_password(user, password)
        idToken = status["idToken"]
        auth.send_email_verification(idToken)
        print("Email de verificação enviado: ", user)

    if opcao == "3":
        user = input("Digite seu e-mail: ")
        password = input("Digite sua senha, com pelo menos 6 caracteres: ")
        status = auth.sign_in_with_email_and_password(user, password)
        idToken = status["idToken"]
        info = auth.get_account_info(idToken)
        users = info["users"]
        verifyEmail = users[0]["emailVerified"]

        if verifyEmail:
            print("Segundo Fator de Autenticao")
            codigo = random.randint(100,1000)
            mail_body = "Código de validação: %d "%codigo

            mensagem = MIMEMultipart()
            mensagem['From'] = username
            mensagem['To'] = user
            mensagem['Subject'] = "Código E-mail"
            mensagem.attach(MIMEText(mail_body, 'plain'))

            connection = smtplib.SMTP(server, port)
            connection.starttls()
            connection.login(username, passwordSMTP)
            connection.send_message(mensagem)
            connection.quit()

            codigoEmail = int(input("Entre com o código que foi enviado por e-mail: "))

            if codigo == codigoEmail:
                print("Usuário Autenticado!!!")
                break
            else:
                print("Código Inválido!!")

        else:
            print("Email não verificado!")



# Alterando a permissão do usuário para leitura, escrita e execução
os.chmod("acesso.txt", stat.S_IRWXU)

print("Registrando log de acesso.")
time.sleep(2)

# Configuração de data, transformando em string para usar no Write
data_login = datetime.now()
data_texto = data_login.strftime("%d/%m/%Y %H:%M")

#Abrindo o arquivo previamente criado, escrevendo nele e fechando o arquivo
arquivo = open("acesso.txt", 'a')
arquivo.write(user + " - ")
arquivo.write(data_texto + "\n")
arquivo.close()

# Alterando a permissão do usuário para somente leitura.
os.chmod("acesso.txt", stat.S_IRUSR)

print("Acesso gravado!")